//
//  ICSegmentVC.swift
//  ICSegmentControl
//
//  Created by cuichengcheng on 2017/12/7.
//  Copyright © 2017年 cuichengcheng. All rights reserved.
//

import UIKit

class ICSegmentVC: UIViewController {

    override func viewDidLoad() {
        super.viewDidLoad()

        self.view.backgroundColor = .white
        
        
        let segContr = ICSegController()
        segContr.seg.segTitles = ["资讯", "视频","政务", "要闻","办公", "工作","风格", "商店","商品", "账单","最近", "没了",]
        
        var vcs:[UIViewController] = []
        for i in 0..<segContr.seg.segTitles.count {
            let vc = ViewController()
            vc.lab.text = segContr.seg.segTitles[i]
            vcs.append(vc)
        }
        segContr.viewControllers = vcs
        segContr.view.frame = CGRect(x: 0, y: 100, width: SCREEN_W, height: SCREEN_H-100)
        self.addChildViewController(segContr)
        self.view.addSubview(segContr.view)
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

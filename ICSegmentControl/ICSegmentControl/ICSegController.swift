//
//  ICSegController.swift
//  ICSegmentControl
//
//  Created by cuichengcheng on 2017/12/7.
//  Copyright © 2017年 cuichengcheng. All rights reserved.
//

import UIKit

class ICSegController: UIViewController, UIScrollViewDelegate {
    let seg = ICSgement()
    let bgSV:UIScrollView = UIScrollView()
    var viewControllers: [UIViewController] = []
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
        self.view.backgroundColor = .white
//        顶部segment
        seg.currentIndicatorTintColor = .orange
        seg.showSeprateLine = true
        //可设置分割线长短 颜色
        //        seg.seprateRatio = 0.5
        //        seg.seprateColor = .cyan
        //自定义cell
        //        seg.customCellClass = ICCustomCollectionViewCell.classForCoder()
        //        let colors:Array<UIColor> = [.red, .orange, .cyan, .gray, .brown]
        //custom cell 加载回调
        seg.customCellForIndexClo = { (cell, index) in
            print(index)
        }
        weak var weakSelf = self
        //cell点击事件
        seg.didSelectClo = { (index) in
            print(weakSelf?.seg.segTitles[index] as Any)
            weakSelf?.bgSV.setContentOffset(CGPoint(x:SCREEN_W*CGFloat(index), y:0), animated: true)
            weakSelf?.loadShowVC(index)
        }
        
        seg.frame = CGRect(x: 0, y: 0, width: SCREEN_W, height: 30)
        self.view.addSubview(seg)
        
        
        //viewController view 载体
        bgSV.isPagingEnabled = true
        bgSV.delegate = self
        self.view.addSubview(bgSV)
        bgSV.frame = CGRect(x: 0, y: seg.frame.maxY, width: SCREEN_W, height: self.view.bounds.height-seg.frame.maxY)
        bgSV.contentSize = CGSize(width: CGFloat(seg.segTitles.count)*SCREEN_W, height: bgSV.bounds.height)
        if viewControllers.count > 0 {
            let showVC = viewControllers.first
            self.addChildViewController(showVC!)
            showVC?.view.frame = bgSV.bounds
            bgSV.addSubview(showVC!.view)
        }
        
        
        if viewControllers.count <= 0 {
            for i in 0..<seg.segTitles.count {
                let v = UIView()
                v.backgroundColor = .randomColor
                v.frame = CGRect(x: CGFloat(i)*SCREEN_W, y: 0, width: SCREEN_W, height: bgSV.contentSize.height)
                bgSV.addSubview(v)
            }
        }        
    }
    
    func scrollViewDidEndDecelerating(_ scrollView: UIScrollView) {
        let index = scrollView.contentOffset.x/SCREEN_W
        let currentIndex = lroundf(Float(index))
        seg.currentSelect(currentIndex, animated: true)
        self.loadShowVC(currentIndex)
        
    }
    
    //load current vc if never loaded
    func loadShowVC(_ index:Int) {
        if index < viewControllers.count {
            let showVC = viewControllers[index]
            if showVC.isViewLoaded == false {
                self.addChildViewController(showVC)
                showVC.view.frame = CGRect(x: CGFloat(index)*SCREEN_W, y: 0, width: SCREEN_W, height: bgSV.contentSize.height)
                bgSV.addSubview(showVC.view)
            }
        }
    }
    
    

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }
    

    /*
    // MARK: - Navigation

    // In a storyboard-based application, you will often want to do a little preparation before navigation
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        // Get the new view controller using segue.destinationViewController.
        // Pass the selected object to the new view controller.
    }
    */

}

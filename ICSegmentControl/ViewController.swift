//
//  ViewController.swift
//  ICSegmentControl
//
//  Created by cuichengcheng on 2017/12/7.
//  Copyright © 2017年 cuichengcheng. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    let lab = UILabel()
    override func viewDidLoad() {
        super.viewDidLoad()
        // Do any additional setup after loading the view, typically from a nib.
        self.view.backgroundColor = .white
        
        self.title = "hello icSegmentControl"
        self.navigationItem.rightBarButtonItem = UIBarButtonItem(title: "push", style: .plain, target: self, action: #selector(goSegmentVC))
        
        lab.text = lab.text != nil ? lab.text : "hello icSegmentControl"
        lab.textAlignment = .center
        lab.font = UIFont.systemFont(ofSize: 22, weight: UIFont.Weight(rawValue: 0.8))
        self.view.addSubview(lab)
        lab.center = self.view.center
        lab.bounds = CGRect(x: 0, y: 0, width: self.view.bounds.size.width, height: 200)
    }
    @objc func goSegmentVC() {
        let vc = ICSegmentVC()
        self.navigationController?.pushViewController(vc, animated: true)
    }
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
        // Dispose of any resources that can be recreated.
    }


}


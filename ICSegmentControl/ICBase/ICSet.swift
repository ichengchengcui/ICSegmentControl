//
//  ICSet.swift
//  ICSegmentControl
//
//  Created by cuichengcheng on 2017/12/7.
//  Copyright © 2017年 cuichengcheng. All rights reserved.
//

import UIKit

class ICSet: NSObject {

}
let statusBarH = UIApplication.shared.statusBarFrame.height
let navH = (statusBarH + 44)

let SCREEN_W:CGFloat = UIScreen.main.bounds.size.width
let SCREEN_H:CGFloat = UIScreen.main.bounds.size.height
/// 适配6s
///
/// - Parameter w:  6s show width
/// - Returns: current screen show width
func ic_scale_w(_ w:CGFloat) -> CGFloat {
    return w*SCREEN_W/414
}
/// - Parameter h:  6s show height
/// - Returns: current screen show height
func ic_scale_h(_ h:CGFloat) -> CGFloat {
    return h*SCREEN_H/736
}
public func RGB(_ rgbValue:Int)->UIColor{
    return UIColor(red: ((CGFloat)((rgbValue & 0xFF0000) >> 16)) / 255.0,
                   green: ((CGFloat)((rgbValue & 0xFF00) >> 8)) / 255.0,
                   blue: ((CGFloat)(rgbValue & 0xFF)) / 255.0,
                   alpha: 1.0)
}
public func RGBA(_ rgbValue:Int,a:CGFloat)->UIColor{
    return UIColor(red: ((CGFloat)((rgbValue & 0xFF0000) >> 16)) / 255.0,
                   green: ((CGFloat)((rgbValue & 0xFF00) >> 8)) / 255.0,
                   blue: ((CGFloat)(rgbValue & 0xFF)) / 255.0,
                   alpha: a)
}
public func RGB(r:CGFloat,g:CGFloat,b:CGFloat)->UIColor{
    return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: 1.0)
}
public func RGBA(r:CGFloat,g:CGFloat,b:CGFloat,a:CGFloat)->UIColor{
    return UIColor(red: r/255.0, green: g/255.0, blue: b/255.0, alpha: a)
}
public func RGBOne(_ rgb:CGFloat)->UIColor{
    return UIColor(red: rgb/255.0, green: rgb/255.0, blue: rgb/255.0, alpha: 1.0)
}
func getFont(_ size:CGFloat) -> UIFont {
    return UIFont.systemFont(ofSize: ic_scale_w(size))
}
func getFont(_ size:CGFloat ,weight:CGFloat) -> UIFont {
    if #available(iOS 8.2, *) {
        return UIFont.systemFont(ofSize: ic_scale_w(size), weight: UIFont.Weight(rawValue: ic_scale_w(weight)))
    }
    return UIFont.systemFont(ofSize: ic_scale_w(size))
}
let onePix = 1.0/UIScreen.main.scale


func addLeftSpace(_ tf:UITextField,space:CGFloat) {
    let spaceView = UILabel(frame:CGRect(x: 0, y: 0, width: space, height: space))
    tf.leftView = spaceView
    tf.leftViewMode = .always
    
}
func addLeftSearch(_ tf:UITextField,space:CGFloat) {
    let img = #imageLiteral(resourceName: "icon_search_suggest")
    let spaceView = UIView()
    let searchView = UIImageView(frame:CGRect(x: ic_scale_w(10), y: 0, width: space, height: space*(img.size.height/img.size.width)))
    searchView.image = img
    spaceView.frame = CGRect(x: 0, y: 0, width: space+ic_scale_w(16), height: searchView.frame.height)
    spaceView.addSubview(searchView)
    tf.leftView = spaceView
    tf.leftViewMode = .always
    
}
func printLog<T>(_ text:T ,file:String = #file,method: String = #function,line: Int = #line){
    print("\((file as NSString).lastPathComponent)[\(line)],\(method):\(text)")
}




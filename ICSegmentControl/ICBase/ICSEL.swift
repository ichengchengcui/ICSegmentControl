//
//  ICSEL.swift
//  ICSegmentControl
//
//  Created by cuichengcheng on 2017/12/7.
//  Copyright © 2017年 cuichengcheng. All rights reserved.
//

import UIKit

class ICSEL: NSObject {

}

func image(_ img:UIImage , color:UIColor) -> UIImage {
    UIGraphicsBeginImageContextWithOptions(img.size, false, img.scale);
    let context = UIGraphicsGetCurrentContext();
    context?.translateBy(x: 0, y: img.size.height)
    context?.scaleBy(x: 1.0, y: -1.0)
    context?.setBlendMode(CGBlendMode.normal)
    let rect = CGRect(x: 0, y: 0, width: img.size.width, height: img.size.height)
    context?.clip(to: rect, mask: img.cgImage!)
    color.setFill()
    context?.fill(rect)
    let newImg = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext();
    return newImg!
}
func scaleImg(_ img:UIImage,height:CGFloat) -> UIImage {
    
    let width = height/img.size.height * img.size.width
    let scaleSize = CGSize(width: width, height: height)
    
    UIGraphicsBeginImageContext(scaleSize)
    img.draw(in: CGRect(x: 0, y: 0, width: scaleSize.width, height: scaleSize.height))
    let scaleImg = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    if let scaleImg = scaleImg {
        return scaleImg
    }
    print("压缩失败")
    return img
}
func snapImgV(_ v:UIView ,f:CGRect) -> UIImageView {
    UIGraphicsBeginImageContext(f.size)
    let context = UIGraphicsGetCurrentContext()
    v.layer.render(in: context!)
    let targetImg = UIGraphicsGetImageFromCurrentImageContext()
    UIGraphicsEndImageContext()
    let imgV = UIImageView()
    imgV.contentMode = .scaleAspectFit
    imgV.contentScaleFactor = UIScreen.main.scale
    imgV.frame=f
    imgV.image=targetImg
    return imgV
}
func boudingRect(text:String,maxSize:CGSize,font:UIFont) -> CGRect {
    return text.boundingRect(with: maxSize, options:NSStringDrawingOptions.usesLineFragmentOrigin, attributes: [kCTFontAttributeName as NSAttributedStringKey:font], context: nil)
}
/**
 *汉字转拼音 并去空格
 */
func transformToPinyin(str:String) -> String {
    let mutStr = NSMutableString(string: str) as CFMutableString
    if CFStringTransform(mutStr, nil, kCFStringTransformToLatin, false) == true {
        if CFStringTransform(mutStr, nil, kCFStringTransformStripDiacritics, false) == true {
            return deleteWhite(mutStr as String)
        }
        return str
    }
    return str
}
/**
 *删除空格
 */
func deleteWhite(_ str:String) -> String {
    return String(str.filter({$0 != " "}))
}
func isNilStr(str:String?) -> String {
    guard let str = str,str.trimmingCharacters(in: CharacterSet.whitespaces).count>0 else{
        return ""
    }
    return str
}

func getCurrentMonthOfDays()->Int? {
    
    //我们大致可以理解为：某个时间点所在的“小单元”，在“大单元”中的数量
    
    let days = Calendar.current.range(of: .day, in: .month, for: Date())
    return days?.count
}
//计算当月天数
func getDaysIn(year:Int,month:Int) -> Int {
    let calender = NSCalendar.current
    
    var startComps = DateComponents()
    startComps.day = 1
    startComps.month = month
    startComps.year = year
    
    var endComps = DateComponents()
    endComps.day = 1
    endComps.month = month == 12 ? 1 : month + 1
    endComps.year = month == 12 ? year + 1 : year
    
    let startDate = calender.date(from: startComps)
    let endDate = calender.date(from: endComps)
    
    let com = calender.dateComponents([.day], from: startDate!, to: endDate!)
    
    if let days = com.day {
        return days
    }
    return 0
}
//高斯模糊
func loadImg(_ imgSrc:String?)->UIImage?{
    if let imgSrc = imgSrc {
        
        let url = URL(string: imgSrc)
        do {
            let imgData = try Data(contentsOf: url!)
            let img = UIImage(data: imgData)
            return img!
        } catch  {
            return nil
        }
    }
    return nil
}
//获取沙盒文件夹路径
func documentsDirectory()->NSString {
    let paths = NSSearchPathForDirectoriesInDomains(.documentDirectory,
                                                    .userDomainMask, true)
    let documentsDirectory = paths.first!
    return documentsDirectory as NSString
}

//获取数据文件地址
func dataFilePath (_ name:String)->String{
    return documentsDirectory().appendingPathComponent(name)
}

/// 转换json字符串
///
/// - Parameter obj: json...
/// - Returns: string
func changeToJsonString(_ obj:Any?) -> String? {
    do {
        let jsonData = try JSONSerialization.data(withJSONObject: obj!, options: .prettyPrinted)
        let jsonStr = String.init(data: jsonData, encoding: .utf8)
        return jsonStr
    } catch  {
        return nil
    }
}


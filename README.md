# ICSegmentController
segment
自定义segment segmentController
可分开使用
### segment
### 上传记录：
2017.12.08 -- segmentController


--------------------------
## 释
    segment
![Alert text](segment.jpeg)

    //  传入 segTitles 即可设置数据源使用
    //        顶部segment
    
    代码示例
    
    seg.currentIndicatorTintColor = .orange
    seg.showSeprateLine = true
    //可设置分割线长短 颜色
    //        seg.seprateRatio = 0.5
    //        seg.seprateColor = .cyan
    //自定义cell
    //        seg.customCellClass = ICCustomCollectionViewCell.classForCoder()
    //        let colors:Array<UIColor> = [.red, .orange, .cyan, .gray, .brown]
    //custom cell 加载回调
    seg.customCellForIndexClo = { (cell, index) in
    print(index)
    }
    weak var weakSelf = self
    //cell点击事件
    seg.didSelectClo = { (index) in
    print(weakSelf?.seg.segTitles[index] as Any)
    weakSelf?.bgSV.setContentOffset(CGPoint(x:SCREEN_W*CGFloat(index), y:0), animated: true)
    weakSelf?.loadShowVC(index)
    }
    
    seg.frame = CGRect(x: 0, y: 0, width: SCREEN_W, height: 30)
    self.view.addSubview(seg)
    --------------------------------------
    segmentController
![Alert text](segmentController.png)

        //传入ViewControllers 作为数据源 ps：需要和segment segtitles 对应
        加载时只会加载当前要显示vc，以节省资源，加载过不会重新加载
        
        代码示例
        
        let segContr = ICSegController()
        segContr.seg.segTitles = ["资讯", "视频","政务", "要闻","办公", "工作","风格", "商店","商品", "账单","最近", "没了",]
        
        var vcs:[UIViewController] = []
        for i in 0..<segContr.seg.segTitles.count {
        let vc = ViewController()
        vc.lab.text = segContr.seg.segTitles[i]
        vcs.append(vc)
        }
        segContr.viewControllers = vcs
        segContr.view.frame = CGRect(x: 0, y: 100, width: SCREEN_W, height: SCREEN_H-100)
        self.addChildViewController(segContr)
        self.view.addSubview(segContr.view)
    
--------------------------
